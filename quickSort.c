#include<stdio.h>

int partition(int A[], int L, int R)
{
	int pivot = A[R];
	
	int i,j;
	for( i = L-1, j = L; j<R; j++)
	{
		if(A[j] < pivot)
		{
			i++;
			int t = A[i];
			A[i] = A[j];
			A[j] = t;
			
		}
	}

	int t = A[i+1];
	A[i+1] = pivot;
	A[R] = t;

	return i+1;
}

void quickSort(int A[], int L, int R)
{
	if(L >= R)
		return;

	int p = partition(A,L,R);
	quickSort(A,L,p-1);
	quickSort(A,p+1,R);
	
}

void printArray(int A[], int size)
{
	printf("\n");
	for(int i = 0; i < size; i++)
		printf("%d \t", A[i]);
	printf("\n");
}

int main()
{
	int A[5] = {1,3,5,2,4};
	quickSort(A,0,4);
	printArray(A,5);
	
}

