#include<stdio.h>
#include<stdlib.h>

typedef struct node Node;

struct node {
	int data;
	Node *next;
};

Node *create_node(int item, Node *next)
{
	Node *new_node = NULL;
	new_node = malloc(sizeof(Node));
	if(new_node == NULL)
	{
		printf("Couldn't allocate memory for new node! \n");
		exit(1);
	}
	new_node->data = item;
	new_node->next = next;

	return new_node;
}

void printNodeDetail(Node *p)
{
	printf("\nNode contains: \n \t Data: %d \n \t Address of itself: %p \n \t Address of next item: %p \n", p->data, p, p->next);
}

int main()
{

	Node *head;
	Node *nextOne;
	
	head = create_node(5,NULL);
	nextOne = create_node(3,NULL);

	//Initially created Nodes
	//containing no linking address
	printf("Printing Node details before linking. \n");
	printNodeDetail(head);
	printNodeDetail(nextOne);

	head->next = nextOne;

	//After linking one node to head
	printf("Printing Node details after linking. \n");
	printNodeDetail(head);
	printNodeDetail(nextOne);

	//Freeing up memory
	free(head);
	free(nextOne);
	head = NULL;
	nextOne = NULL;
}
