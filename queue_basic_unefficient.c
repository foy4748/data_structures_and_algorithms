#include<stdio.h>

#define QUEUE_MAX 10

typedef struct {
	int head;
	int tail;
	int data[QUEUE_MAX];
} Queue;

void enqueue(Queue *s, int item)
{
	if(s->tail < QUEUE_MAX)
	{
		s->data[s->tail] = item;
		s->tail = s->tail + 1;
	}
	else
		printf("Queue is Full!! \n");
}

int dequeue(Queue *s)
{
	if(s->tail == 0)
	{	
		printf("Queue is Empty!! \n");
		return -1;}
	else
	{
		int item = s->data[s->head];
		s->head = s->head + 1;
		return item;
	}
}

int main()
{
	Queue myQueue;
	myQueue.head = 0;
	myQueue.tail = 0;

	enqueue(&myQueue, 5);
	enqueue(&myQueue, 3);
	enqueue(&myQueue, 7);
	
	int item1 = dequeue(&myQueue);
	int item2 = dequeue(&myQueue);
	printf("Dequeued two items:%d, %d \n", item1, item2);
	enqueue(&myQueue, 12);
	enqueue(&myQueue, 1);
	printf("After dequeing two items and enqueing more two items \n the head and tail is: h: %d, t: %d \n", myQueue.head, myQueue.tail);
}
