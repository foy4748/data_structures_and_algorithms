/*------------------------------
Implementing a binary tree as given below
      _  2 _			    ---Level 0
     /      \
		/		     \
	 7		      9		    ---Level 1
 /		\        \
1      6        8	    ---Level 2
		 /   \     /	\
    5    10   3		 4	---Level 3

	
 ------------------------------*/
#include<stdio.h>
#include<stdlib.h>

typedef struct tree_node Node;

struct tree_node {
	int data;
	Node *left_child;
	Node *right_child;
};

Node *create_tree_node(int data)
{
	Node *new_node = NULL;
	new_node = malloc(sizeof(Node));
	new_node->data = data;
	new_node->left_child = NULL;
	new_node->right_child = NULL;

	return new_node;
}

void add_left_child(Node *root, Node *child)
{
	root->left_child = child;
}

void add_right_child(Node *root, Node *child)
{
	root->right_child = child;
}

void printParentDetail(Node *n)
{
	printf("The details of this parent: \n");
	printf("\t Address of itsefl: %p \n", n);
	printf("\t Data stored: %d \n", n->data);

	printf("\n\t Address of Left Child: %p \n", n->left_child);	
	if((n->left_child) != NULL)
		printf("\t Data of Left Child: %d \n", (n->left_child)->data);

	printf("\n\t Address of Right Child: %p \n", n->right_child);
	if((n->right_child) != NULL)
		printf("\t Data of Right Child: %d \n", (n->right_child)->data);
}

Node *create_tree(Node *root)//Level 0
{
	Node *left1, *right1;	//Level 1
	Node *left2_1, *right2_1, *right2_2; //Level 2
	Node *left3_1, *right3_1, *left3_2, *right3_2; //Level 3

	//Assiging data to each nodes
	root = create_tree_node(2);
	left1 = create_tree_node(7);
	right1 = create_tree_node(9);
	left2_1 = create_tree_node(1);
	right2_1 = create_tree_node(6);
	right2_2 = create_tree_node(8);
	left3_1 = create_tree_node(5);
	right3_1 = create_tree_node(10);
	left3_2 = create_tree_node(3);
	right3_2 = create_tree_node(4);

	//Linking nodes to their parents
	
	//Level 1
	add_left_child(root, left1);
	add_right_child(root, right1);

	//Level 2
	add_left_child(left1, left2_1);
	add_right_child(left1, right2_1);
	add_right_child(right1, right2_2);

	//Level 3
	add_left_child(right2_1, left3_1);
	add_right_child(right2_1, right3_1);
	add_left_child(right2_2, left3_2);
	add_right_child(right2_2, right3_2);

	return root;
}

//TRAVERSING FUNCTIONS ----------
void preorderTraverse(Node *n)
{
	//printParentDetail(n);
	printf("%d \n", n->data);

	if(n->left_child != NULL)
		preorderTraverse(n->left_child);
	if(n->right_child != NULL)
		preorderTraverse(n->right_child);
}

void postorderTraverse(Node *n)
{
	//printParentDetail(n);

	if(n->left_child != NULL)
		preorderTraverse(n->left_child);
	
	if(n->right_child != NULL)
		preorderTraverse(n->right_child);

	printf("%d \n", n->data);
}

void inorderTraverse(Node *n)
{
	//printParentDetail(n);

	if(n->left_child != NULL)
		preorderTraverse(n->left_child);

	printf("%d \n", n->data);

	if(n->right_child != NULL)
		preorderTraverse(n->right_child);
}

//END OF TRAVERSING FUNCTIONS ----------

int main()
{
	Node * root = create_tree_node(0); //Created a NULL root node

	root = create_tree(root); //Then creating the given tree and populating root

	preorderTraverse(root);

}
