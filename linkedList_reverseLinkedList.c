#include<stdio.h>
#include<stdlib.h>

typedef struct node Node;

struct node {
	int data;
	Node *next;
};

Node *create_node(int item, Node *next)
{
	Node *new_node = NULL;
	new_node = malloc(sizeof(Node));
	if(new_node == NULL)
	{
		printf("Couldn't allocate memory for new node! \n");
		exit(1);
	}
	new_node->data = item;
	new_node->next = next;

	return new_node;
}

Node *reverseLinkedList(Node *n)
{
	if(n == NULL || n->next ==  NULL)
		return n;
	
	//When Base case is triggered
	//It returns the node at the end
	//since its next node is NULL
	//Puts the end node at the new head
	//and Pops the stack!!
	Node *new_head = reverseLinkedList(n->next);

	n->next->next = n;
	n->next = NULL;
	
	return new_head;
	
}

void printNodeDetail(Node *p)
{
	printf("\nNode contains: \n \t Data: %d \n \t Address of itself: %p \n \t Address of next item: %p \n", p->data, p, p->next);
}

int main()
{

	Node *head;
	Node *nextOne;
	
	head = create_node(5,NULL);
	nextOne = create_node(3,NULL);

	//Initially created Nodes
	//containing no linking address
	printf("Printing Node details before linking. \n");
	printNodeDetail(head);
	printNodeDetail(nextOne);

	head->next = nextOne;

	//After linking one node to head
	printf("Printing Node details after linking. \n");
	printNodeDetail(head);
	printNodeDetail(nextOne);

	printf("After reversal: \n");
	Node *p = reverseLinkedList(head);
	printNodeDetail(p);

	//Freeing up memory
	free(head);
	free(nextOne);
	head = NULL;
	nextOne = NULL;
}
