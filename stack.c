#include<stdio.h>

#define STACK_MAX 10

typedef struct {
	int top;
	int data[STACK_MAX];
} Stack;

void push(Stack *s, int item)
{
	if(s->top < STACK_MAX)
	{
		s->data[s->top] = item;
		s->top = s->top + 1;
	}
	else
		printf("Stack is Full!! \n");
}

int pop(Stack *s)
{
	if(s->top == 0)
	{	
		printf("Stack is Empty!! \n");
		return -1;
	}
	else
	{
		s->top = s->top - 1;
		int item = s->data[s->top];
		return item;
	}
}

int main()
{
	Stack myStack;
	myStack.top = 0;

	push(&myStack, 5);
	int item1 = pop(&myStack);
	int item2 = pop(&myStack);
	printf("The poped two items are %d %d \n", item1, item2);
}
