#include<stdio.h>

#define Q_Size 3

typedef struct {
	int head, tail;
	int data[Q_Size + 1];
} Queue;

void enqueue(Queue *q, int item)
{
	if( q->tail - q->head == Q_Size )
	{
		printf("The queue is Full!! \n");
		return;
	}
	else {
		q->data[q->tail % (Q_Size +1)] = item;
		q->tail = (q->tail + 1) % (Q_Size + 1);
		printf("Enqueued %d successfully \n",item);
	}
}

int dequeue(Queue *q)
{
	if(q->head == q->tail)
	{
		printf("The ques is empty!! \n");
	}
	else
	{
		int item = q->data[q->head];
		q->head = (q->head + 1) % (Q_Size + 1);
		return item;
	}
}

int main()
{
	Queue myQueue;
	myQueue.head = myQueue.tail = 0;
	enqueue(&myQueue, 5);
	enqueue(&myQueue, 2);
	enqueue(&myQueue, 7);

	enqueue(&myQueue, 6);

	int item1 = dequeue(&myQueue);
	printf("%d \n", item1);
	int item2 = dequeue(&myQueue);
	printf("%d \n", item2);
	int item3 = dequeue(&myQueue);
	printf("%d \n", item3);

	enqueue(&myQueue, 6);
}
