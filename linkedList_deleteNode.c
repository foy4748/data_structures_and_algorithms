#include<stdio.h>
#include<stdlib.h>

typedef struct node Node;

struct node {
	int data;
	Node *next;
};

Node *create_node(int item, Node *next)
{
	Node *new_node = NULL;
	new_node = malloc(sizeof(Node));
	if(new_node == NULL)
	{
		printf("Couldn't allocate memory for new node! \n");
		exit(1);
	}
	new_node->data = item;
	new_node->next = next;

	return new_node;
}

Node *delete_node(Node *head, Node *to_be_deleted)
{
	if(head == to_be_deleted)
	{
		head = to_be_deleted->next;
		free(to_be_deleted);
		return head;
	}

	Node *cursor = head; //Cursor for selecting the node before the to_be_deleted node
	while(cursor != NULL)
	{
		if(cursor->next == to_be_deleted) break;
		cursor = cursor->next;
	}

	if(cursor == NULL)
		return head;
	cursor->next = to_be_deleted->next;
	free(to_be_deleted);

	return head;
}

void printNodeDetail(Node *p)
{
	printf("\nNode contains: \n \t Data: %d \n \t Address of itself: %p \n \t Address of next item: %p \n", p->data, p, p->next);
}

int main()
{

	Node *head;
	Node *second;
	Node *third;
	
	head = create_node(5,NULL);
	second = create_node(3,NULL);
	third = create_node(9,NULL);

	//Initially created Nodes
	//containing no linking address
	printf("Printing Node details before linking. \n");
	printNodeDetail(head);
	printNodeDetail(second);
	printNodeDetail(third);

	head->next = second;
	second->next = third;

	//After linking one node to head
	printf("Printing Node details after linking. \n");
	printNodeDetail(head);
	printNodeDetail(second);
	printNodeDetail(third);

	//TEST 1
	/*Testing: Deleting head node
	Node *newHead = delete_node(head,head);

	printf("\n The new head address: %p \n", newHead);
	printNodeDetail(second);
	*/
	
	//TEST 2
	/*Testing: Deleting any node
	Node *newHead = delete_node(head,second);

	printf("\n Now the head contains address of third node: %p \n", newHead);
	printNodeDetail(head);
	*/
	
	//Comment this free up segment while
	//testing deletion
	
	//Freeing up memory
	free(head);
	free(second);
	free(third);
	head = NULL;
	second = NULL;
	third = NULL;
}
