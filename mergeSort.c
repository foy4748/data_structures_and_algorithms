#include<stdio.h>

void merge(int A[], int L, int M, int R)
{
	//Splitting Given Array
	int size_Left = M - L + 1;
	int size_Right = R - M;

	int LEFT[size_Left], RIGHT[size_Right];

	for(int i = 0; i<M+1; i++)
		LEFT[i] = A[i+L];
	
	for(int i = 0; i<R; i++)
		RIGHT[i] = A[i+M+1];
	//END of Spitting ---------------------
	
	//Merging
	int i = 0;
	int j = 0;
	int k = L;

	while(i < size_Left && j < size_Right)
	{
		if(LEFT[i] < RIGHT[j])
		{
			A[k] = LEFT[i];
			i++;
		}
		else
		{
			A[k] = RIGHT[j];
			j++;
		}
		k++;
	}

		//Pusing the rest of the elements
	while( i < size_Left )
	{
		A[k] = LEFT[i];
		i++;
		k++;
	}
	while( j < size_Right )
	{
		A[k] = RIGHT[j];
		j++;
		k++;
	}
		//END of pushing ---------------------
	//END of Merging ---------------------
}

void printArray(int A[], int size)
{
	printf("\n");
	for(int i = 0; i < size; i++)
		printf("%d \t", A[i]);
	printf("\n");
}

void mergeSort(int A[], int L, int R)
{
	if(L < R)
	{
		int M = L + (R - L)/2;
		mergeSort(A,L,M);
		mergeSort(A, M+1, R);

		merge(A,L,M,R);
	}
}


int main()
{
	int A[5] = {1,3,5,2,4};
	mergeSort(A,0,4);
	printArray(A,5);
	
}
